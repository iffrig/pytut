\documentclass{article}

% Auteur : Olivier Iffrig
% Copyright : 2014 Olivier Iffrig
% Licence : CC-BY-SA (http://creativecommons.org/licenses/by-sa/4.0/)

\usepackage[utf8]{inputenc}
\usepackage[greek,french]{babel}
\usepackage[usenames,svgnames]{xcolor}
\usepackage[pdftex]{hyperref}
\usepackage{palatino}
\usepackage{graphicx}
\usepackage{listings}
\usepackage{tikz}
\usepackage{geometry}

\geometry{hmargin=1.5in}

\hypersetup{colorlinks,%
            %citecolor=black,%
            %filecolor=black,%
            linkcolor=blue,%
            urlcolor=blue%
            }

\title{Le codage des caractères en Python}
\author{Olivier Iffrig}
\date{\today}

\lstset{literate=
  {á}{{\'a}}1 {é}{{\'e}}1 {í}{{\'i}}1 {ó}{{\'o}}1 {ú}{{\'u}}1
  {Á}{{\'A}}1 {É}{{\'E}}1 {Í}{{\'I}}1 {Ó}{{\'O}}1 {Ú}{{\'U}}1
  {à}{{\`a}}1 {è}{{\`e}}1 {ì}{{\`i}}1 {ò}{{\`o}}1 {ù}{{\`u}}1
  {À}{{\`A}}1 {È}{{\'E}}1 {Ì}{{\`I}}1 {Ò}{{\`O}}1 {Ù}{{\`U}}1
  {ä}{{\"a}}1 {ë}{{\"e}}1 {ï}{{\"i}}1 {ö}{{\"o}}1 {ü}{{\"u}}1
  {Ä}{{\"A}}1 {Ë}{{\"E}}1 {Ï}{{\"I}}1 {Ö}{{\"O}}1 {Ü}{{\"U}}1
  {â}{{\^a}}1 {ê}{{\^e}}1 {î}{{\^i}}1 {ô}{{\^o}}1 {û}{{\^u}}1
  {Â}{{\^A}}1 {Ê}{{\^E}}1 {Î}{{\^I}}1 {Ô}{{\^O}}1 {Û}{{\^U}}1
  {œ}{{\oe}}1 {Œ}{{\OE}}1 {æ}{{\ae}}1 {Æ}{{\AE}}1 {ß}{{\ss}}1
  {ç}{{\c c}}1 {Ç}{{\c C}}1 {ø}{{\o}}1 {å}{{\r a}}1 {Å}{{\r A}}1
  {€}{{\EUR}}1 {£}{{\pounds}}1
}
\lstset{ %
    language=Python, %
    keywordstyle=\color{DarkBlue}, %
    keywordstyle=[2]\color{DarkGreen}, %
    morekeywords={as,with}, %
    morekeywords=[2]{bytes}, %
    stringstyle=\color{DarkRed}, %
    showstringspaces=false %
}
\lstdefinelanguage{PythonError}{%
    breaklines=true,%
    sensitive=true,%
    keywordstyle=\color{Red},%
    keywordstyle=[2]\color{Orange},
    morekeywords={ArithmeticError,AssertionError,AttributeError,BaseException,%
        BufferError,EOFError,EnvironmentError,Exception,FloatingPointError,%
        GeneratorExit,IOError,ImportError,IndentationError,IndexError,KeyError,%
        KeyboardInterrupt,LookupError,MemoryError,NameError,%
        NotImplementedError,OSError,OverflowError,ReferenceError,RuntimeError,%
        StandardError,StopIteration,SyntaxError,SystemError,SystemExit,TabError,%
        TypeError,UnboundLocalError,UnicodeDecodeError,UnicodeEncodeError,%
        UnicodeError,UnicodeTranslateError,ValueError,ZeroDivisionError},%
    morekeywords=[2]{BytesWarning,DeprecationWarning,FutureWarning,%
        ImportWarning,PendingDeprecationWarning,RuntimeWarning,SyntaxWarning,%
        UnicodeWarning,UserWarning,Warning}%
    }

\newcommand\gpic[3]{\begin{center}\includegraphics[#1]{#2}\\* #3\end{center}}
\newcommand\cpic[2]{\gpic{height=4cm}{#1}{#2}}
\newcommand\Cpic[2]{\cpic{#1}{\rule{0pt}{2ex} \tiny #2}}
\newcommand\pic[1]{\cpic{#1}{}}

\newcommand\vcpic[2]{\gpic{width=4cm}{#1}{#2}}
\newcommand\vCpic[2]{\vcpic{#1}{\rule{0pt}{2ex} \tiny #2}}
\newcommand\vpic[1]{\vcpic{#1}{}}

\newcommand\cc[1]{{\footnotesize #1}}

\begin{document}
\maketitle

\section*{Licence}

Cet article, ainsi que les images qu'il contient (sauf mention contraire
explicite) sont sous licence Creative Commons CC-BY-SA. Vous pouvez le copier et
le modifier à votre guise, à condition de citer l'auteur, de mettre en évidence
vos modifications et de partager les modifications sous la même licence. Pour
plus de détails : \url{http://creativecommons.org/licenses/by-sa/4.0/}

\section{Le co--- quoi ?}

Nos ordinateurs ne comprennent que le binaire, c'est à dire des 0 et des
1\footnote{pouvant être représentés physiquement de diverses manières, par
exemple un potentiel électrique inférieur ou supérieur à un seuil donné},
souvent regroupés par 8 (les octets).

\pic{../../../pic/bonjour.pdf}

Pour pouvoir représenter du texte dans ce système, il faut donc choisir une
re\-pré\-sen\-ta\-tion pour chaque caractère. C'est ce qu'ont fait un certain
nombre de gens, et vous vous imaginez bien qu'ils ne se sont pas concertés
avant, du coup, à la fin des années 50, chacun avait sa propre convention de
codage des caractères.

Afin d'arranger un peu les choses, l'ISO a décidé en 1960 de créer un comité
chargé des systèmes d'information, dont l'un des objectifs était de coordonner
les différentes conventions de codage. C'est ainsi que naît l'\emph{American
Standard Code for Information Interchange}, abrégé ASCII.

\begin{center}
    \resizebox{.8\textwidth}{!}{
        \begin{tabular}{c|cccccccccccccccc}
            & .0 & .1 & .2 & .3 & .4 & .5 & .6 & .7 & .8 & .9 & .A & .B & .C & .D & .E & .F \\
            \hline
            0. & \cc{NUL} & \cc{SOH} & \cc{STX} & \cc{ETX} & \cc{EOT} & \cc{ENQ} & \cc{ACK} &
              \cc{BEL} & \cc{BS} & \cc{HT} & \cc{LF} & \cc{VT} & \cc{FF} & \cc{CR} &
              \cc{SO} & \cc{SI} \\
            1. & \cc{DLE} & \cc{DC1} & \cc{DC2} & \cc{DC3} & \cc{DC4} & \cc{NAK} &
              \cc{SYN} & \cc{ETB} & \cc{CAN} & \cc{EM} & \cc{SUB} & \cc{ESC} &
              \cc{FS} & \cc{GS} & \cc{RS} & \cc{US} \\
            2. & \cc{SP} & ! & " & \# & \$ & \% & \& & ' & ( & ) & * & + & , & - & . & / \\
            3. & 0 & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 & : & ; & $<$ & = & $>$ & ? \\
            4. & @ & A & B & C & D & E & F & G & H & I & J & K & L & M & N & O \\
            5. & P & Q & R & S & T & U & V & W & X & Y & Z & [ & \textbackslash & ] & \^{} & \_ \\
            6. & \`{} & a & b & c & d & e & f & g & h & i & j & k & l & m & n & o \\
            7. & p & q & r & s & t & u & v & w & x & y & z & \{ & | & \} & \~{} & \cc{DEL}
        \end{tabular}
    }\\*[.5ex]
    La table des caractères ASCII
\end{center}

L'ASCII permet de représenter sur\hspace{0.1em} 7 bits (que l'on préfixe en
général par un 0 pour com\-plé\-ter l'octet) les caractères usuels de la langue
anglaise, c'est-à-dire les lettres majuscules et minuscules, les chiffres,
quelques symboles de ponctuation, ainsi que des caractères de contrôle servant
à modifier le comportement des terminaux\footnote{Oui, à l'époque, les
ordinateurs étaient quelque peu encombrants, donc les opérateurs disposaient
d'un clavier et d'un écran permettant de les contrôler depuis un bureau.}.

\Cpic{../../../pic/accent.pdf}{UnicodeDecodeError: 'ascii' codec can't decode
    byte 0xc3 in position 7: ordinal not in range(128)}

Étant donné que beaucoup de langues utilisent des caractères ne figurant pas
dans \mbox{l'ASCII}, des extensions ont été proposées afin de pallier ce manque.
Une fois de plus, les diverses extensions n'étaient pas toujours compatibles
entre elles, et nous voilà revenus à notre point de départ.

\cpic{../../../pic/xkcd_standards.png}{\copyright XKCD, \url{http://xkcd.com/927}}

Bref, il existe beaucoup de codages possibles pour représenter du texte. Et je
ne parle même pas de le manipuler. Imaginons que vous voulez réécrire un mot en
lettres capitales. Tant qu'il s'agit de caractères ASCII, il suffit d'enlever 32 aux
caractères entre 97 et 122 (les lettres minuscules). Mais comment faire si c'est
une autre lettre ? Ça dépend du codage utilisé. Et rien ne nous garantit que la
capitale correspondante peut elle aussi être représentée. Et puis, qu'est-ce qui
me dit que je ne suis pas en train d'utiliser une lettre différente dessinée de
la même façon ?

\vCpic{../../../pic/pas_un_i.pdf}{U+0399 GREEK CAPITAL LETTER IOTA}

Tous ces problèmes trouvent une solution avec le standard Unicode, qui associe
d'une part un nom à chaque caractère (abstrait), et d'autre part un numéro,
appelé \emph{point de code}. Ainsi, si on connaît la correspondance entre une
convention de codage et les points de code de chacun des caractères, la
manipulation devient plus aisée.

% 'c7 61 20 61 6c 6f 72 73 a0 21 20 4a 65 20 70 65 75 78 20 e9 63 72 69 72 65 20
% 64 65 73 20 63 61 72 61 63 74 e8 72 65 73 20 61 63 63 65 6e 74 75 e9 73 a0 21'

De manière à pouvoir représenter aisément les caractères Unicode, on peut bien
sûr utiliser le codage que l'on veut. Il en existe cependant trois qui ont été
prévus de manière à faciliter les choses. Il s'agit d'UTF-8, UTF-16 et UTF-32,
utilisant comme unité de base 8, 16 ou 32 bits (je dis bien unité de base, parce
qu'un caractère peut nécessiter plusieurs unités), et qui peuvent encoder
n'importe quel caractère Unicode.


\section{Et Python dans tout ça ?}

En Python 2.x, il existe deux types de chaînes de caractères : \lstinline{str}
et \lstinline{unicode}. Les deux peuvent servir à représenter des caractères.
Comme vous l'aurez deviné, \lstinline{unicode} sert à représenter des chaînes de
caractères Unicode. Par opposition, comme son nom ne l'indique pas,
\lstinline{str} devrait servir à représenter des suites d'octets, dont certaines
sont, de manière fortuite, des représentations encodées d'une chaîne de
caractères. En Python 3, la notation est clarifiée puisque les chaînes Unicode
sont rebaptisées \lstinline{str}, alors que les suites d'octets prennent le type
\lstinline{bytes}.

\begin{center}
    \begin{tabular}{r@{}cc|ccccc}
        & Python 2 & Python 3 & \greektext σ & \greektext o & \greektext f & \greektext 'i & \greektext a \\
        \hline
        \tikz[remember picture] \node (n1) {}; & \lstinline{unicode} & \lstinline{str}   & 03C3 & 03BF & 03C6 & 03AF & 03B1 \tikz[remember picture] \node (n3) {};\\
        \tikz[remember picture] \node (n2) {}; & \lstinline{str}     & \lstinline{bytes} & CF83 & CEBF & CF86 & CEAF & CEB1 \tikz[remember picture] \node (n4) {};\\
    \end{tabular}\\*[.5ex]
    Un exemple en UTF-8
    \begin{tikzpicture}[remember picture, overlay, >=stealth]
        \path[->] (n1.west) edge[bend right] node[midway, left]  {\lstinline{.encode("utf-8")}} (n2.west);
        \path[->] (n4.east) edge[bend right] node[midway, right] {\lstinline{.decode("utf-8")}} (n3.east);
    \end{tikzpicture}
\end{center}

En pratique, pour passer d'une chaîne d'octets à une chaîne Unicode, il faut
utiliser la méthode \lstinline{decode}, qui prend en argument le nom du codage
utilisé (et aussi la manière de traiter les erreurs, je vous laisse lire la
documentation si ça vous intéresse). Le passage d'une chaîne Unicode à une
chaîne d'octets se fait via la méthode \lstinline{encode} qui fonctionne de la
même façon.

\begin{lstlisting}
b = "\xc3\xa9" # un "é" en UTF-8
s = b.decode("utf-8") # un "é" représenté en Unicode (U+00E9)
S = s.upper() # un "É" représenté en Unicode (U+00C9)
B = S.encode("utf-8") # un "É" en UTF-8 (C389)
\end{lstlisting}

Lorsqu'on veut représenter du texte, il est donc fortement recommandé d'utiliser
des chaînes Unicode le plus longtemps possible, et de ne choisir un encodage que
pour importer ou exporter le texte (dans un fichier ou via un réseau, par
exemple). Que ce soit pour du texte ou des suites d'octets, il est primordial de
bien savoir ce que l'on manipule afin d'éviter les confusions.  Surtout en
Python 2 où le passage entre \lstinline{str} et \lstinline{unicode} peut être
implicite (et source d'erreurs cryptiques et difficiles à repérer).

\cpic{../../../pic/sandwich.pdf}{Le \og sandwich unicode \fg}

\subsection{Mauvaises pratiques}

\begin{enumerate}
    \item Une fonction \og universelle \fg{} de conversion (Python 2.x, a son
        équivalent en Python 3.x)
        \begin{lstlisting}
def convert_everything_to_unicode(x):
    if isinstance(x, unicode):
        return x
    else:
        return str(x).decode('utf-8')
        \end{lstlisting}

        Cette fonction n'est pas fondamentalement mauvaise, puisqu'elle sert à
        convertir une chaîne d'octets en chaîne Unicode. Cependant, elle n'aide
        en rien à connaître le contenu de \lstinline{x}, et donc devient dangereuse
        si utilisée n'importe où. De plus, elle présuppose que la chaîne
        d'octets encode un texte en UTF-8, ce qui n'est pas né\-ces\-sai\-re\-ment le
        cas (sauf convention d'usage, qui doit dans ce cas être clairement
        explicitée). Enfin, elle opère une conversion en chaîne d'octets, ce qui
        ouvre la porte à beaucoup de mauvaises utilisations. Même remarque pour
        la fonction inverse :

        \begin{lstlisting}
def convert_everything_to_str(x):
    if isinstance(x, str):
        return x
    else:
        return unicode(x).encode('utf-8')
        \end{lstlisting}

    \item Mélanger \lstinline{str} et \lstinline{unicode} (Python 2.x
        uniquement)
        \begin{lstlisting}
def print_result(r):
    print u"Résultat : " + str(r)
        \end{lstlisting}

        Cette fonction présente deux points de danger : premièrement,
        l'opérateur \lstinline{+} opéré entre un \lstinline{str} et un
        \lstinline{unicode}, qui entraîne une conversion implicite du
        \lstinline{str} en \lstinline{unicode}. Si par malheur,
        \lstinline{str(r)} renvoie une chaîne d'octets contenant des octets de
        valeur numérique supérieure à 127, une erreur serait lancée :

        \begin{lstlisting}[language=PythonError]
UnicodeDecodeError: 'ascii' codec can't decode byte 0xc3 in position 0: ordinal not in range(128)
        \end{lstlisting}

        Deuxièmement, l'erreur inverse risque de se produire si l'encodage de la
        chaîne Unicode demandé implicitement par \lstinline{print} échoue
        (souvent parce que l'encodage de la sortie standard n'est pas connu ou
        est plus restrictif que nécessaire) :
        \begin{lstlisting}[language=PythonError]
UnicodeEncodeError: 'ascii' codec can't encode character u'\xe9' in position 0: ordinal not in range(128)
        \end{lstlisting}

        Ces problèmes sont résolus en Python 3.x, où aucune conversion entre
        \lstinline{bytes} et \lstinline{str} n'est faite implicitement. On a
        donc une erreur lorsqu'on essaie par exemple de concaténer une chaîne
        d'octets et une chaîne de caractères Unicode, quel que soit le contenu
        des chaînes.
\end{enumerate}

\subsection{Quelques conseils}

\begin{enumerate}
    \item Fichiers texte

        En Python 2.x, on peut manipuler des fichiers texte à l'aide du module
        \lstinline{codecs} qui permet de spécifier un codage lors de l'ouverture
        du fichier :

        \begin{lstlisting}
import codecs

with codecs.open("toto.txt", "w", encoding="utf-8") as f:
    f.write(u"Enchant\u00e9.\n")
        \end{lstlisting}

        Attention cependant, en utilisant le module \lstinline{codecs}, on perd
        la conversion automatique des fins de ligne.
        
        En Python 3.x, la fonction \lstinline{open} dispose d'un argument
        \lstinline{encoding} qui permet d'utiliser directement ce que permettait
        \lstinline{codecs.open} en Python 2.x. On peut également noter que les
        retours à la ligne peuvent être convertis automatiquement, contrairement
        aux fichiers ouverts avec le module \lstinline{codecs}.

        \begin{lstlisting}
with open("toto.txt", "w", encoding="utf-8") as f:
    f.write("Enchant\u00e9.\n")
        \end{lstlisting}

    \item Déclarez un codage si possible

        Pour de nombreux dispositifs d'entrée-sortie, il est possible de
        déclarer un encodage :

        \begin{lstlisting}
# -*- coding: cp1252 -*-
        \end{lstlisting}
        \begin{lstlisting}[language=xml]
<?xml version="1.0" encoding="iso-8859-1"?>
        \end{lstlisting}
        \begin{lstlisting}[language=html]
<meta http-equiv="Content-Type"
    content="text/html;charset=utf-8"?>
        \end{lstlisting}

        Si vous utilisez un codage fixe, n'oubliez pas de préciser la convention
        dans la documentation de votre code.

        Attention cependant, vous ne devez (et ne pouvez) pas faire confiance
        aux données venant de l'extérieur. Il se peut très bien que le codage
        annoncé ne permette pas de décoder l'entrée correspondante.

        \begin{lstlisting}
data_raw="""
<?xml version="1.0" encoding="utf-8"?>
<junk>\xff\x00\x11\x22</junk>
"""
data = data_raw.decode("utf-8")
        \end{lstlisting}
        \begin{lstlisting}[language=PythonError]
UnicodeDecodeError: 'utf8' codec can't decode byte 0xff in position 46: invalid start byte
        \end{lstlisting}

        N'essayez pas pour autant de deviner l'encodage à utiliser. Prévoyez
        simplement l'éventualité de manière à ce que l'exception levée ne
        perturbe pas le comportement de votre code.

\end{enumerate}

\section{Conclusion}

Maintenant que vous avez les bases, il ne vous reste qu'à voler de vos propres
ailes. Comme partout, c'est en essayant et en faisant des erreurs qu'on apprend.
Et surtout, \emph{testez votre code} avec des entrées volontairement
pathologiques (UTF-8 non valide, codage annoncé différent du codage réel,
caractères non-ASCII, caractères multi-octets, etc.).

Un lien utile, qui a grandement inspiré cet article :
\url{http://nedbatchelder.com/text/unipain.html}

\end{document}
